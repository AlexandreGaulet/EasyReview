"use client"

import BoxLearn from "./BoxLearn"

export default function LearnWithHidden() {
    return (
        <div className='h-screen w-screen flex justify-center pt-20'>
             <BoxLearn/>
        </div>
    )
}